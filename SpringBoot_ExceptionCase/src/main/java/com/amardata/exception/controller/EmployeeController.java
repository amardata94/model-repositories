package com.amardata.exception.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.amardata.exception.service.EmployeeService;
import com.amarnath.rentcloud.rencloud_commons.model.Employee;

@RestController
@RequestMapping(value = "/api")
public class EmployeeController {

    private static final Logger LOGGER=LoggerFactory.getLogger(EmployeeController.class);
	
	@Autowired
	private EmployeeService employeeService;

	@GetMapping(value = "/employee/{employeeId}")
	public Employee getEmployeeById(@PathVariable Long employeeId) throws handleRuntimeExceptionWithId {
		LOGGER.info("get Employee by employeeId called :"+employeeId);
		if (employeeId <= 0) {
			throw new handleRuntimeExceptionWithId("Employee is not found with id" + employeeId);
		}

		return this.employeeService.findById(employeeId);
	}

	@GetMapping(value = "/allEmployee")
	public List<Employee> getListOfEmployee() {
		LOGGER.info("Get List of Employee Record :");
		return employeeService.findAllEmployee();
	}

	@PostMapping(value = "/saveEmployee")
	public Employee saveEmployeeDetails(@RequestBody Employee emp) {
		LOGGER.info("Save Employee Called :"+ emp);
		return employeeService.saveEmployee(emp);
	}

	@PutMapping("/employees/{employeeId}")
	public ResponseEntity<Employee> updateEmployee(@PathVariable Long employeeId, @RequestBody Employee employeeDetails)
			throws handleRuntimeExceptionWithId {
		LOGGER.info("Update Employee Called :" + employeeId);
		Employee employee=employeeService.findById(employeeId);
		
		if (employeeId <= 0) {
			throw new handleRuntimeExceptionWithId("Employee is not found with id" + employeeId);
		}
		
		employee.setName(employeeDetails.getName());
		employee.setSalary(employeeDetails.getSalary());
		employee.setAddress(employeeDetails.getAddress());
		
		//save employee
		final Employee updateEmployee = employeeService.saveEmployee(employee);
		
		return ResponseEntity.ok(updateEmployee);
	}

	@DeleteMapping(value = "/deleteEmployee/{employeeId}")
	public void deleteEmployeeById(@Param("employeeId") Long employeeId) throws handleRuntimeExceptionWithId {
		LOGGER.info("delete employee by id called :"+employeeId);
		if (employeeId <= 0) {
			throw new handleRuntimeExceptionWithId("Employee is not found with id" + employeeId);
		}

		employeeService.deleteEmployeeById(employeeId);
	}
}
