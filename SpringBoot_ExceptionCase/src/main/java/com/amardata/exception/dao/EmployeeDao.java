package com.amardata.exception.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.amarnath.rentcloud.rencloud_commons.model.Employee;


@Repository
public interface EmployeeDao extends JpaRepository<Employee, Long> {

	@Modifying
	@Query(value = "delete from Employee u where u.employeeId LIKE %:employeeId%", nativeQuery = true)
    void deleteEmployeeById(Long employeeId);
}
