package com.amardata.exception;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
@EntityScan(basePackages = "com.amarnath.rentcloud.rencloud_commons.model")
public class SpringBootExceptionCaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootExceptionCaseApplication.class, args);
	}

}
