package com.amardata.exception.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.amarnath.rentcloud.rencloud_commons.model.Employee;


@Service
public interface EmployeeService {

	public Employee saveEmployee(Employee employee);
	
	public Employee findById(Long employeeId);
	
	//public Employee findByName(Long employeeId);
	
	public List<Employee> findAllEmployee();
	
	public void deleteEmployeeById(Long employeeId);
	
	
}
