package com.amardata.exception.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amardata.exception.dao.EmployeeDao;
import com.amarnath.rentcloud.rencloud_commons.model.Employee;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeDao employeeDao;

	@Override
	public Employee saveEmployee(Employee employee) {
		return employeeDao.save(employee);
	}

	@Override
	public Employee findById(Long employeeId) {
		return employeeDao.findById(employeeId).get();
	}

	@Override
	public List<Employee> findAllEmployee() {
		return employeeDao.findAll();
	}

	@Override
	@Transactional
	public void deleteEmployeeById(Long employeeId) {
		employeeDao.deleteEmployeeById(employeeId);
		
	}




}
