package com.amardata.exception.exceptionHandler;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.HandlerMapping;

@ControllerAdvice
public class ApplicationExceptionHandler {

	/*
	 * @ExceptionHandler(ResourceNotFoundException.class) public ResponseEntity<?>
	 * handleGenericException(ResourceNotFoundException e, WebRequest request) {
	 * EmployeeException employeeException = new EmployeeException(100,
	 * "Employees are not found", request.getDescription(false));
	 * 
	 * return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
	 * employeeException);
	 * 
	 * }
	 * 
	 */
	// applicable for all type of exception
	/*
	 * @ExceptionHandler(Exception.class) public ResponseEntity<?>
	 * globalExceptionHandler(Exception ex, WebRequest request) { EmployeeException
	 * message = new EmployeeException(HttpStatus.INTERNAL_SERVER_ERROR.value(),
	 * ex.getMessage(), request.getDescription(false));
	 * 
	 * return new ResponseEntity<EmployeeException>(message,
	 * HttpStatus.INTERNAL_SERVER_ERROR); }
	 */

	/*
	 * @ExceptionHandler(RuntimeException.class) public ResponseEntity<?>
	 * handleRuntimeException(Exception e, WebRequest request) { EmployeeException
	 * employeeException = new EmployeeException(101, "Employee is not found",
	 * request.getDescription(false)); return
	 * ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
	 * employeeException);
	 * 
	 * }
	 */
	// particular "employee_id" is not found

	@ExceptionHandler(RuntimeException.class)
	public ResponseEntity<?> handleRuntimeExceptionWithId(RuntimeException e, HttpServletRequest request,
			WebRequest request1) {
		final Map<String, String> pathVariables = (Map<String, String>) request
				.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
		EmployeeException employeeException = new EmployeeException(101,
				String.format("Employee with employeeId-%s is not found", pathVariables.get("employeeId")),
				request1.getDescription(false));
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(employeeException);

	}

}
