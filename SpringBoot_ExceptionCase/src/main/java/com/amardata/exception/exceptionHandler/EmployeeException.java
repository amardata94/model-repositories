package com.amardata.exception.exceptionHandler;

public class EmployeeException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int erroeCode;
	private String message;
	private String description;

	public EmployeeException() {
		super();
	}

	public EmployeeException(int erroeCode, String message, String description) {
		super();
		this.erroeCode = erroeCode;
		this.message = message;
		this.description=description;
	}

	public int getErroeCode() {
		return erroeCode;
	}

	public void setErroeCode(int erroeCode) {
		this.erroeCode = erroeCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
